class Article {
  String title;
  String author;
  String description;
  String urlToImage;
  DateTime publishedAt;
  String content;
  String url;

  Article(
      {this.title,
        this.description,
        this.publishedAt,
        this.author,
        this.content,
        this.urlToImage,
        this.url,
        });

}

