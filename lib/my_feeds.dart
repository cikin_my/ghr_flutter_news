import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ghr_news_feeds/news.dart';

//import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:like_button/like_button.dart';
import 'package:share/share.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class MyFeeds extends StatefulWidget {
  @override
  _DiscoverPageState createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<MyFeeds> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  static const String placeholderImg = 'assets/images/png/loading.gif';
  bool loadingInProgress;
  var newslist;
  double screenHeight;

  void getNews() async {
    News news = News();
    await news.getNews();
    setState(() {
      loadingInProgress = false;
      newslist = news.news;
    });
  }

  @override
  void initState() {
    loadingInProgress = true;
    // TODO: implement initState
    super.initState();
    getNews();
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text('News Feed'),
        backgroundColor: Colors.blue[900],
      ),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 8.0,
        ),
        child: loadingInProgress
            ? Center(
                child: Text('loading ...'),
              )
            : Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
//                    Text(
//                      'Top HeadLines',
//                      style: TextStyle(
//                        fontSize: 30.0,
//                        letterSpacing: 2,
//                        fontWeight: FontWeight.bold,
//                      ),
//                    )
                    ],
                  ),
                  Divider(),
                  Expanded(
                    child: ListView.builder(
                        itemCount: newslist.length,
                        itemBuilder: (context, index) {
//                  UI design start here
                          return Card(
                              elevation: 5,
                              margin: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 10.0),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Column(
                                children: [
                                  //========== news row ========
                                  Column(
                                    children: [
                                      ClipRRect(
                                        clipBehavior: Clip.antiAlias,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10.0),
                                          topRight: Radius.circular(10.0),
                                        ),
                                        child: Align(
                                          alignment: Alignment.bottomCenter,
                                          child: CachedNetworkImage(
                                            alignment: Alignment.topCenter,
                                            placeholder: (context, url) => Image.asset(placeholderImg),
                                            imageUrl: newslist[index].urlToImage,
                                            height: screenHeight / 3,
                                            width: double.maxFinite,
                                            fit: BoxFit.cover,
                                            errorWidget: (context, url, error) => Image.asset(
                                              'assets/images/png/loading.gif',
                                            ),
                                          ),
                                        ),
                                      ),
                                      InkWell(
                                        splashColor: Colors.lightBlue.shade50,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 8.0, top: 8.0),
                                              child: Text(
                                                newslist[index].title,
                                                style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.w900,
                                                  fontFamily: 'NewsCycle',
                                                  height: 1.2,
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 4.0),
                                              child: Text(
                                                newslist[index].description,
                                                style: TextStyle(
                                                  color: Colors.grey[500],
                                                  fontSize: 16,
                                                  fontFamily: 'roboto',

                                                ),
                                              ),
                                            ),

                                          ],
                                        ),
                                        onTap: () {
                                          flutterWebViewPlugin.launch(newslist[index].url);
                                        },
                                      ),
                                    ],
                                  ),
                                  //======== date row ==========
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(left: 10.0),
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              'Resource Name',
                                              style: TextStyle(
                                                fontSize: 13,
                                                fontFamily: 'NewsCycle',
                                              ),
                                            ),
                                            SizedBox(
                                              height: 15,
                                              child: VerticalDivider(
                                                thickness: 1,
                                                color: Colors.blueGrey[100],
                                              ),
                                            ),
                                            Text(
                                              timeago.format((newslist[index].publishedAt)),
                                              style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Colors.grey[600],
                                                fontFamily: 'NewsCycle',
                                              ),
                                            ),
                                            SizedBox(
                                              height: 15,
                                              child: VerticalDivider(
                                                thickness: 1,
                                                color: Colors.blueGrey[100],
                                              ),
                                            ),
                                            LikeButton(
                                              size: 24,
                                              circleColor: CircleColor(start: Color(0xff00ddff), end: Color(0xff0099cc)),
                                              bubblesColor: BubblesColor(
                                                dotPrimaryColor: Color(0xff33b5e5),
                                                dotSecondaryColor: Color(0xff0099cc),
                                              ),
                                              likeBuilder: (bool isLiked) {
                                                return Icon(
                                                  Icons.thumb_up,
                                                  color: isLiked ? Colors.blue[800] : Colors.grey,
                                                  size: 24,
                                                );
                                              },
                                              likeCount: 665,
                                              countBuilder: (int count, bool isLiked, String text) {
                                                var color = isLiked ? Colors.blue[800] : Colors.grey;
                                                Widget result;
                                                if (count == 0) {
                                                  result = Text(
                                                    "love",
                                                    style: TextStyle(color: color),
                                                  );
                                                } else
                                                  result = Text(
                                                    text,
                                                    style: TextStyle(color: color),
                                                  );
                                                return result;
                                              },
                                            ),

                                            IconButton(
                                              padding: EdgeInsets.all(0),
                                              color: Colors.blue[300],
                                              icon: Icon(
                                                FontAwesomeIcons.retweet,
                                                size: 20,
                                              ),
                                              onPressed: () {
                                                final RenderBox box = context.findRenderObject();
                                                Share.share(newslist[index].url, sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ));
                        }),
                  )
                ],
              ),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Add your onPressed code here!
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.blue[700],
      ),
    );
  }
}
