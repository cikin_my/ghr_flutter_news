import 'package:ghr_news_feeds/article.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class News {
  List<Article> news = [];

  String apikey = 'c8c2861011564c11b279035973605d65';
  String headline = 'top-headlines';

  Future<void> getNews() async {

    String url = 'http://newsapi.org/v2/top-headlines?country=my&apiKey=c8c2861011564c11b279035973605d65';

    var response = await http.get(url);
    var jsonData = jsonDecode(response.body);

    if (jsonData['status'] == 'ok') {
      jsonData['articles'].forEach((element) {
        if (element['urlToImage'] != null) {
          Article article = Article(
            title: element['title'],
            author: element['author'],
            description: element['description'],
            urlToImage: element['urlToImage'],
            content: element['content'],
            url: element['url'],
            publishedAt: DateTime.parse(element['publishedAt']),
          );
          news.add(article);



        }

      });

    } else {
      print('error retrieving data');
    }



  }
}
